import Backbone from '~/../node_modules/backbone/backbone';
import objectTableTemplate from '~/template/object-table.html';
import ObjectCollection from '~/scripts/model/object-collection';
import StateCollection from '~/scripts/model/state-collection';
import ObjectTableRow from './object-table-row';
import $ from 'jquery';

export default class ObjectTable extends Backbone.View {
  constructor() {
    super();

    this.objectCollection = new ObjectCollection();
    this.collectionLoaded = this.objectCollection.fetchInterval(2000).then(() => {
      this.listenTo(this.objectCollection, 'add', (model) => this._objectAdd(model) );
    });

    this.stateCollection = new StateCollection();
    this.statesLoaded = this.stateCollection.fetch();
  }

  render() {
    this.$el.html('Loading...');

    Promise.all([this.collectionLoaded, this.statesLoaded ]).then(() => {
      this.$el.html(objectTableTemplate());

      this.$tbody = this.$('.js-object-list');

      this.objectCollection.each((model) => {
        new ObjectTableRow({ model, stateCollection: this.stateCollection }).render().$el.appendTo(this.$tbody);
      });
    });


    return this;
  }

  _objectAdd(model) {
    const index = this.objectCollection.indexOf(model);
    const beforeElement = this.$tbody.children().get(index);
    const $newElement = new ObjectTableRow({ model, stateCollection: this.stateCollection }).render().$el;

    if (beforeElement) {
      $(beforeElement).before($newElement);

      return;
    }

    this.$tbody.append($newElement);
  }

  remove() {
    this.objectCollection.stopFetchInterval();

    super.remove();
  }
}