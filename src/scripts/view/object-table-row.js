import Backbone from '~/../node_modules/backbone/backbone';
import objectTableTemplate from '~/template/object-table-row.html';

export default class ObjectTableRow extends Backbone.View {
  constructor(options) {
    super(options);

    this.stateCollection = options.stateCollection;

    this.listenTo(this.model, 'remove', () => this.remove());
    this.listenTo(this.model, 'change', () => this.render());
  }

  get tagName() {
    return 'tr';
  }

  render() {

    const state = this.stateCollection.get(this.model.get('state_id'));

    const stateName = state.get('name');
    const stateUi = state.get('ui');

    this.$el.html(objectTableTemplate({
      name: this.model.get('name'),
      status: stateName
    }));


    this.$('.js-status').css( { backgroundColor: stateUi.colors.main } );

    return this;
  }
}