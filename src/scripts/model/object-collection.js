import BaseCollection from './base-collection';

export default class ObjectCollection extends BaseCollection {
  url() {
    return '/node/api/objects';
  }

  // get comparator() {
  //   return 'name';
  // }
}