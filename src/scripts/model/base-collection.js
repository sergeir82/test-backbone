import Backbone from '~/../node_modules/backbone/backbone';
import { Base64 } from 'js-base64';

export default class BaseCollection extends Backbone.Collection {
  fetch(options = {}) {
    const headers = Object.assign({}, options.headers || {}, {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + Base64.encode(
        // `sergey.ratnikov@saymon.info:sergeir82`
        `admin:saymon`
      )
    });

    return super.fetch(Object.assign({}, options, { headers }));
  }

  fetchInterval(period, options = {}) {
    this.stopFetchInterval();
    this.interval = setInterval(() => {
      this.fetch(options);
    }, period);

    return this.fetch(options);
  }

  stopFetchInterval() {
    this.interval && clearInterval(this.interval);
  }
}
