import BaseCollection from './base-collection';

export default class StateCollection extends BaseCollection {
  url() {
    return '/node/api/states';
  }
}